#! /bin/bash

echo "Current home directory: $HOME"
echo "Current user name: $USER"
echo "Today is: $(date +%D)"
echo "No of users logged in: $(who | wc -l)"
