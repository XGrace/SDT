#! /bin/bash

array=()

loop=0

while [ $loop -lt 10 ]; do
	echo "Enter in number $((loop+1)):"
	read n
	array[$loop]=$n
	loop=$((loop+1))
done

for i in ${array[*]}; do
	if [ $i -ge 0 ]; then
		echo "Number $i is positive"
	else
		echo "Number $i is negative"
	fi
done

