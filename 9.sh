#! /bin/bash

echo "Enter in a number to find out if it is even or odd:"
read n

if [ $((n % 2)) -eq 0 ]; then
	echo "$n is even"
else
	echo "$n is odd"
fi
