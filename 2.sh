#! /bin/bash

first=0
second=1
sequence=0

if [ -n "$1" ]; then
	n=$1
else
	n=100
fi

until [ $n -eq 0 ]; do
	sequence=$((first+second))
	first=$second
	second=$sequence
	n=$((n-1))
done
echo $sequence
