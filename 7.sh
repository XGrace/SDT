#! /bin/bash

echo "This is a simple calculator."

loop=1
while [ $loop ]; do
	echo "Enter in 2 numbers (or enter 'q' for either to quit)"
	read num1
	read num2

	if [[ "$num1" == "q" || "$num2" == "q" ]]; then
		exit
	fi
	
	echo "Enter in the operation you want to perform:"
	printf "1: Addition\n2: Subtraction\n3: Multiplication\n4: Division\n"
	read operation
	
	case $operation in
		1 ) echo "The result of addition is $((num1 + num2))"
			;;
		2 ) echo "The result of subtraction is $((num1 - num2))"
			;;
		3 ) echo "The result of multiplication is $((num1 * num2))"
			;;
		4 ) echo "The result of division is $((num1 / num2))"
			;;
	esac
done 
