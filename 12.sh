#! /bin/bash

for ((i=0;i<10;i++)); do
	echo "Enter in number $((i+1)):"
	read numbers[i]
done

for i in ${numbers[*]}; do
	if [ $((i % 2)) -eq 0 ]; then
		echo "Number $i is even! Writing to even.txt"
		echo "$i" >> even.txt
	else
		echo "Number $i is odd! Writing to odd.txt"
		echo "$i" >> odd.txt
	fi
done
