
#! /bin/bash

echo "Enter in a string to find out if it is a palindrome or not"
read string

if [ $(rev <<< $string) == $string ]; then
	echo "$string is a palindrome. Adding to palindrome.txt"
	echo "$string" >> palindrome.txt
else
	echo "$string is not a palindrome"
fi

