#! /bin/bash

if [ -n "$1" ]; then
	n=$1
else
	echo "Enter in a number to check if it is prime or not:"
	read n
fi

i=2

while [ $i -lt $n ]; do
	if [ $((n % i)) -eq 0 ]; then
		echo "Number $n is not prime"
		exit
	fi
	i=$((i+1))
done

echo "$n is a prime number"
