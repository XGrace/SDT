#! /bin/bash

if [ -n "$1" ]; then
	n=$1
else
	echo "Enter in the number to calculate the factorial of:"
	read n
fi

factorial=1

until [ $n -eq 0 ]; do
	factorial=$((factorial*n))
	n=$((n-1))
done

echo $factorial

