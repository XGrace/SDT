#! /bin/bash

echo "Enter in 3 numbers to find the max out of all three."

loop=0

until [ $loop -eq 3 ]; do
	echo "Enter in number $((loop+1)):"
	read numbers[$loop]
	loop=$((loop + 1))
done

max=0

for i in ${numbers[*]}; do
	if [ $max -lt $i ]; then
		max=$i
	fi
done

echo "The max number is $max"
