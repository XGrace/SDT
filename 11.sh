#! /bin/bash

echo "Enter in 3 scores from Math, English, and Science subjects respectively to calculate the average score between them."

echo "Enter in Math test score:"
read math

echo "Enter in English test score:"
read english

echo "Enter in Science test score:"
read science

average=$(( (math + english + science) / 3))
echo "The scores are: Math = $math, English = $english, Science = $science"
echo "The average of these values is $average"

if [[ $average -ge 90 && $average -le 100 ]]; then
	echo "Grade A"
elif [[ $average -ge 80 && $average -lt 90 ]]; then
	echo "Grade B"
elif [[ $average -ge 70 && $average -lt 80 ]]; then
	echo "Grade C"
elif [[ $average -ge 60 && $average -lt 70 ]]; then
	echo "Grade D"
else
	echo "Grade F"
fi
